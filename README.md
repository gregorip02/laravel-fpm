# PHP FPM

Este repositorio contiene los archivos de construcción de la imagen
`gregorip02/laravel-fpm`, util para correr aplicaciones php basado en la
tecnología PHP FastCGI Proccess Manager.

Puede utilizar esta imagen para correr sus aplicaciones de Laravel. Tenga en cuenta
que esta imagen **no incluye un servidor web** así que tendrá que construirlo usted
mismo.

## Hello world!

``` bash
# Run a hello world example.
$ docker run -it --rm --name fpm \
  gregorip02/laravel-fpm:8.0 \
  php -r "echo \"Hello world from PHP 8.0\n\";"
```
