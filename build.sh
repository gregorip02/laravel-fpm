#!/usr/bin/env sh

set -o errexit
set -o nounset

IMAGE_NAME="gregorip02/laravel-fpm"

build_image() {
  # Stop script execution if the image variant not found.
  if ! [ ${1:-} ]; then
    echo "We need a image variant for build"
    exit 1
  fi

  # Stop script execution if the dockerfile file does not exist.
  if ! [ -f ${2:-} ]; then
    echo "$2: not souch file or directory"
    exit 1
  fi;

  docker build --pull -t $1 -f $2 .
}

php=$1

echo "Building variant with PHP ${php}"

# $CI_COMMIT_BRANCH and $CI_COMMIT_SHORT_SHA are variables of the
# build environment "gitlab-runner".
if [ $CI_COMMIT_BRANCH = "dev" ]; then
  IMAGE_VARIANT="${IMAGE_NAME}:${php}-dev-${CI_COMMIT_SHORT_SHA}"
else
  IMAGE_VARIANT="${IMAGE_NAME}:${php}"
fi;

build_image $IMAGE_VARIANT "${php}/Dockerfile"
docker push $IMAGE_VARIANT

echo "Done"
